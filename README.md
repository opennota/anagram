anagram [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![Pipeline status](https://gitlab.com/opennota/anagram/badges/master/pipeline.svg)](https://gitlab.com/opennota/anagram/commits/master)
=======

## Install

    go install gitlab.com/opennota/anagram@latest

## Use

    $ anagram австралопитек
    ватерполистка
    
    $ anagram яркий 2
    кий яр
    киряй
    яр кий

